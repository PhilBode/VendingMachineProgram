import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class StorageSlotTest
{
    static StorageSlot slot;

    @BeforeAll
    static void setUp()
    {
        Drink drink = new Drink("Water", 1,"H20");
        slot = new StorageSlot(drink);

    }

    @Test
    void refillProduct()
    {
        slot.refillProduct();
        assertEquals(10, slot.getCurrentAmount());
    }

    @Test
    void removeAll()
    {
        slot.removeAll();
        assertNotEquals(10, slot.getCurrentAmount());
    }

    @Test
    void takeDrink()
    {
        assertEquals("Water", slot.getStoredDrink().getName());
    }

    @Test
    void getCurrentAmount()
    {
        assertEquals(0, slot.getCurrentAmount());
        slot.refillProduct();
        assertEquals(10, slot.getCurrentAmount());
    }
}