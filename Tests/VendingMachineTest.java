import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VendingMachineTest
{
    static VendingMachine machine;

    @BeforeAll
    static void setUp() throws Exception
    {
        machine = new VendingMachine("Neubeckum", 100, 5);

        Drink coke = new Drink("Coca Cola", 2, "1298");
        Drink pepsi = new Drink("Pepsi Cola", 1.90, "1299");
        Drink sprite = new Drink("Sprite", 1.20, "3896");
        Drink fanta = new Drink("Fanta", 0.80, "3546");
        Drink spezi = new Drink("Spezi", 1.40, "8648");

        machine.addDrinkToSlots(coke);
        machine.addDrinkToSlots(pepsi);
        machine.addDrinkToSlots(sprite);
        machine.addDrinkToSlots(fanta);
        machine.addDrinkToSlots(spezi);

        machine.refillChange();
        machine.restockMachine();
    }

    @Test
    void buyDrink()
    {
        var wallet = new Wallet();
        assertDoesNotThrow(() -> wallet.addCoinsToSlot(Coins.Euro1, 2));
        assertThrows(Exception.class, () -> machine.buyDrink("WrongCode", wallet), "Put in the wrong code");
        assertDoesNotThrow(() -> machine.buyDrink("1299", wallet), "Put in the right code now");
    }

    @Test
    void restockMachine()
    {
        machine.emptyMachine();
        machine.restockMachine();
        var wallet = new Wallet();
        assertDoesNotThrow(() -> wallet.addCoinsToSlot(Coins.Euro2, 2));
        assertDoesNotThrow(() -> machine.buyDrink("1299", wallet));
    }

    @Test
    void emptyMachine()
    {
        machine.emptyMachine();
        var wallet = new Wallet();
        assertDoesNotThrow(() -> wallet.addCoinsToSlot(Coins.Euro2, 2));
        assertThrows(Exception.class, () -> machine.buyDrink("1299", wallet), "Cannot buy anything from an empty machine");
    }

    @Test
    void getBalance()
    {
        assertNotEquals(0, machine.getBalance());
    }


}