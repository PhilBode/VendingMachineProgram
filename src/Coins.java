public enum Coins
{
    Euro2(200),
    Euro1(100),
    Cent50(50),
    Cent20(20),
    Cent10(10);

    private final int Value;
    Coins(int cents)
    {
        Value = cents;
    }

    public int getValue()
    {
        return Value;
    }

}
