public interface IVendingMachine
{
    TradeResponse buyDrink(String code, Wallet wallet) throws Exception;

    void restockMachine() throws Exception;
    void refillChange();
    void emptyMachine();
}
