public interface IWallet
{
    void addCoinsToSlot(Coins coin, int amount) throws Exception;
    void setAmountOfCoinsInSlot(Coins coin, int amount);
    void removeCoinsFromSlot(Coins coin, int amount) throws Exception;
    TradeResponse processPayment(StorageSlot slot, Wallet wallet) throws Exception;
    double getBalance();

}
