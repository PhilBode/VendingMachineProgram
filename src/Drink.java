public class Drink
{
    private final String Name;
    private final double Price;
    private final String Code;

    public Drink(String name, double price, String code)
    {
        Name = name;
        Price = price;
        Code = code;
    }

    public String getName()
    {
        return Name;
    }

    public double getPrice()
    {
        return Price;
    }

    public String getCode()
    {
        return Code;
    }
}
