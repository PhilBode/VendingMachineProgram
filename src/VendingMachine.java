public class VendingMachine implements IVendingMachine
{
    private final Wallet wallet;
    private final StorageSlot[] slots;
    private final int maxSlots;
    private final String location;

    public VendingMachine(String location, int maxCoins, int slots) throws Exception
    {
        if (maxCoins < 1 || slots < 1)
            throw new Exception("The vending should keep at least 1 coin and should own 1 slot");

        this.location = location;
        this.wallet = new Wallet(maxCoins);
        this.slots = new StorageSlot[slots];
        this.maxSlots = slots;
    }

    public TradeResponse buyDrink(String code, Wallet wallet) throws Exception
    {
        //Find the slot for the drink
        StorageSlot slot = null;
        for (var currentSlot : slots)
        {
            if (currentSlot.getStoredDrink().getCode().equals(code))
                slot = currentSlot;
        }

        if (slot == null)
            throw new Exception("Drink not found!");

        if (slot.getCurrentAmount() == 0)
            throw new Exception("Drink is out of stock!");


        return this.wallet.processPayment(slot, wallet);
    }

    public void restockMachine()
    {
        for (StorageSlot slot : slots)
        {
            if (slot != null)
                slot.refillProduct();
        }
    }

    public void refillChange()
    {
        wallet.setAmountOfCoinsInSlot(Coins.Euro2, 10);
        wallet.setAmountOfCoinsInSlot(Coins.Euro1, 10);
        wallet.setAmountOfCoinsInSlot(Coins.Cent50, 10);
        wallet.setAmountOfCoinsInSlot(Coins.Cent20, 10);
        wallet.setAmountOfCoinsInSlot(Coins.Cent10, 10);
    }

    public void addDrinkToSlots(Drink drink) throws Exception
    {
        var freeSlotId = -1;
        for (int i = 0; i < slots.length; i++)
        {
            if (slots[i] == null)
            {
                freeSlotId = i;
                break;
            }
        }

        if (freeSlotId == -1)
            throw new Exception("No free slot available!");

        slots[freeSlotId] = new StorageSlot(drink);
    }

    public double getBalance()
    {
        return wallet.getBalance();
    }

    public void emptyMachine()
    {
        for (StorageSlot slot : slots)
            slot.removeAll();
    }

    public String getLocation()
    {
        return location;
    }

    public int getMaxSlots()
    {
        return maxSlots;
    }
}
