public class StorageSlot
{
    private final Drink storedDrink;
    private int currentAmount = 0;

    public StorageSlot(Drink drink)
    {
        storedDrink = drink;
    }

    public void refillProduct()
    {
        currentAmount = 10;
    }

    public void removeAll()
    {
        currentAmount = 0;
    }

    public Drink takeDrink() throws Exception
    {
        if (currentAmount == 0)
            throw new Exception("Ran out of drinks!");

        currentAmount--;
        return storedDrink;
    }

    public Drink getStoredDrink()
    {
        return storedDrink;
    }

    public int getCurrentAmount()
    {
        return currentAmount;
    }
}
