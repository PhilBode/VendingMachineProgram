import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Wallet implements IWallet
{
    private HashMap<Coins, Integer> coinCollection;
    private final int coinLimit;

    public Wallet()
    {
        coinLimit = -1;
        InitializeCoinCollection();
    }

    public Wallet(int limit)
    {
        coinLimit = limit;
        InitializeCoinCollection();
    }

    private void InitializeCoinCollection()
    {
        coinCollection = new HashMap<>();
        coinCollection.put(Coins.Euro2, 0);
        coinCollection.put(Coins.Euro1, 0);
        coinCollection.put(Coins.Cent50, 0);
        coinCollection.put(Coins.Cent20, 0);
        coinCollection.put(Coins.Cent10, 0);
    }


    public void addCoinsToSlot(Coins coin, int amount) throws Exception
    {
        if (getCurrentCoinAmount() + amount > coinLimit && coinLimit > -1)
            throw new Exception("Too many coins for the wallet to handle!");

        coinCollection.put(coin, coinCollection.get(coin) + amount);
    }

    public void setAmountOfCoinsInSlot(Coins coin, int amount)
    {
        coinCollection.put(coin, amount);
    }

    public void removeCoinsFromSlot(Coins coin, int amount) throws Exception
    {
        if (getCoinCountFor(coin) - amount < 0)
            throw new Exception("Not enough change!");

        coinCollection.put(coin, coinCollection.get(coin) - amount);
    }


    public TradeResponse processPayment(StorageSlot slot, Wallet wallet) throws Exception
    {
        if (this.getCurrentCoinAmount() + wallet.getCurrentCoinAmount() > coinLimit)
            throw new Exception("The Wallet cannot hold that many coins!");

        CashInPayment(wallet);
        var pendingChange = CalculateChange(slot.getStoredDrink(), wallet);

        Wallet change = GetChangeAsWallet(pendingChange);
        return new TradeResponse(change, slot.takeDrink());
    }

    private void CashInPayment(Wallet payment)
    {
        for (Map.Entry<Coins, Integer> coin : payment.coinCollection.entrySet())
        {
            var coinAmount = coinCollection.get(coin.getKey());
            coinCollection.put(coin.getKey(), coinAmount + coin.getValue());
        }
    }

    private Wallet GetChangeAsWallet(double change) throws Exception
    {
        var result = new Wallet();

        TreeMap<Coins, Integer> sorted = new TreeMap<>(coinCollection);

        for (Map.Entry<Coins, Integer> coin : sorted.entrySet())
        {
            boolean nextCase = false;
            var currentCoinValue = coin.getKey().getValue();

            while (!nextCase)
            {
                var coinsAvailable = coin.getValue();
                if (change >= currentCoinValue && coinsAvailable > 0)
                {
                    result.addCoinsToSlot(coin.getKey(), 1);
                    removeCoinsFromSlot(coin.getKey(), 1);
                    change -= currentCoinValue;
                } else
                    nextCase = true;
            }
        }

        if (change == 0)
            return result;
        else
            throw new Exception("Not enough change!");
    }

    private double CalculateChange(Drink drink, Wallet wallet)
    {
        var costInCents = drink.getPrice() * 100;
        var paidInCents = wallet.getBalance();

        if (paidInCents < costInCents)
            return paidInCents;

        return paidInCents - costInCents;
    }

    public double getBalance()
    {
        var balance = 0;
        for (Map.Entry<Coins, Integer> collection : coinCollection.entrySet())
        {
            var coin = collection.getKey().getValue();
            var amount = collection.getValue();
            if (amount > 0)
                balance += (coin * amount);
        }
        return balance;
    }

    private int getCoinCountFor(Coins coin)
    {
        return coinCollection.get(coin);
    }

    private double getCurrentCoinAmount()
    {
        var coins = 0;
        for (var currentCoin : coinCollection.entrySet())
            coins += currentCoin.getValue();

        return coins;
    }
}
