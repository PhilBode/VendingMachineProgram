public class TradeResponse
{
    public Wallet Change;
    public Drink Drink;

    public TradeResponse(Wallet wallet, Drink drink)
    {
        Change = wallet;
        Drink = drink;
    }
}
